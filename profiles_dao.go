package dao

import (
	. "bitbucket.org/nycework/model"
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)
type ProfilesDAO struct {
	Session *mgo.Session
	Database string
}

const (
	PROFILE_COLLECTION = "profile"
)

// Establish a connection to database
func (m *ProfilesDAO) Connect() {
	db = m.Session.DB(m.Database)
}

// Find list of Users
func (m *ProfilesDAO) FindAll() ([]Profile, error) {
	var profiles []Profile
	err := db.C(PROFILE_COLLECTION).Find(bson.M{}).All(&profiles)
	return profiles, err
}

// Find a User by its id
func (m *ProfilesDAO) FindById(id string) (Profile, error) {
	var profile Profile
	err := db.C(PROFILE_COLLECTION).FindId(bson.ObjectIdHex(id)).One(&profile)
	return profile, err
}

// Find a User by its id
func (m *ProfilesDAO) FindByPid(aid string) (Profile, error) {
	var profile Profile
	err := db.C(PROFILE_COLLECTION).Find(bson.M{"aid":aid}).One(&profile)
	return profile, err
}

// Insert a User into database
func (m *ProfilesDAO) Insert(profile Profile) error {
	err := db.C(PROFILE_COLLECTION).Insert(&profile)
	return err
}

// Delete an existing User
func (m *ProfilesDAO) Delete(profile Profile) error {
	err := db.C(PROFILE_COLLECTION).Remove(&profile)
	return err
}

// Update an existing User
func (m *ProfilesDAO) Update(profile Profile) error {
	err := db.C(PROFILE_COLLECTION).UpdateId(profile.ID, &profile)
	return err
}