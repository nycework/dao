package dao

import (
	. "bitbucket.org/nycework/model"
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

type AccountsDAO struct {
	Session  *mgo.Session
	Database string
}

const (
	ACCOUNT_COLLECTION = "account_info"
)

// Establish a connection to database
func (m *AccountsDAO) Connect() {
	db = m.Session.DB(m.Database)
}

// Find list of Accounts
func (m *AccountsDAO) FindAll() ([]OrgAccount, error) {
	var accounts []OrgAccount
	err := db.C(ACCOUNT_COLLECTION).Find(bson.M{}).All(&accounts)
	return accounts, err
}

// Find a Account by its id
func (m *AccountsDAO) FindById(id string) (OrgAccount, error) {
	var account OrgAccount
	err := db.C(ACCOUNT_COLLECTION).FindId(bson.ObjectIdHex(id)).One(&account)
	return account, err
}

// Find a Account by its pid
func (m *AccountsDAO) FindByPId(pid string) (OrgAccount, error) {
	var account OrgAccount
	err := db.C(ACCOUNT_COLLECTION).Find(bson.M{"pid": pid}).Sort("-$natural").Limit(1).One(&account)
	return account, err
}

// Insert a Account into database
func (m *AccountsDAO) Insert(account OrgAccount) error {
	for _, key := range []string{"pid", "organization"} {
		index := mgo.Index{
			Key:        []string{key},
			Unique:     true,
			DropDups:   true,
			Background: true, // See notes.
			Sparse:     true,
		}
		if err := db.C(ACCOUNT_COLLECTION).EnsureIndex(index); err != nil {
			return err
		}
	}
	err := db.C(ACCOUNT_COLLECTION).Insert(&account)
	return err
}

// Delete an existing Account
func (m *AccountsDAO) Delete(account OrgAccount) error {
	err := db.C(ACCOUNT_COLLECTION).Remove(&account)
	return err
}

// Update an existing Account
func (m *AccountsDAO) Update(account OrgAccount) error {
	err := db.C(ACCOUNT_COLLECTION).UpdateId(account.ID, &account)
	return err
}


/*############################# Seller Account ##############################*/

// GetAll returns the list of seller
func (m *AccountsDAO) FindAllSeller(collection string) ([]Selller, error) {
	var sellers []Selller
	err := db.C(collection).Find(bson.M{}).All(&sellers)
	return sellers, err
}
// Add Seller in the DB
func (m *AccountsDAO) InsertSeller(collection string,seller Selller) error {
	for _, key := range []string{"slug", "phone", "business_name"} {
		index := mgo.Index{
			Key:        []string{key},
			Unique:     true,
			DropDups:   true,
			Background: true, // See notes.
			Sparse:     true,
		}
		if err := db.C(collection).EnsureIndex(index); err != nil {
			return err
		}
	}

	err := db.C(collection).Insert(&seller)
	return err
}
