package dao

import (
	. "bitbucket.org/nycework/model"
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

type AuthsDAO struct {
	Session  *mgo.Session
	Database string
}

var db *mgo.Database

const (
	AUTH_COLLECTION = "user_info"
)

// Establish a connection to database
func (m *AuthsDAO) Connect() {
	db = m.Session.DB(m.Database)
}

// Find list of Users
func (m *AuthsDAO) FindAll() ([]Account, error) {
	var accounts []Account
	err := db.C(AUTH_COLLECTION).Find(bson.M{}).All(&accounts)
	return accounts, err
}

// Find a User by its id
func (m *AuthsDAO) FindById(id string) (Account, error) {
	var account Account
	err := db.C(AUTH_COLLECTION).FindId(bson.ObjectIdHex(id)).One(&account)
	return account, err
}

// Find a User by its id
func (m *AuthsDAO) FindByUsername(username string) (Account, error) {
	var account Account
	err := db.C(AUTH_COLLECTION).Find(bson.M{"username": username}).One(&account)
	return account, err
}

// Insert a User into database
func (m *AuthsDAO) Insert(account Account) error {
	for _, key := range []string{"username", "phone"} {
		index := mgo.Index{
			Key:        []string{key},
			Unique:     true,
			DropDups:   true,
			Background: true, // See notes.
			Sparse:     true,
		}
		if err := db.C(AUTH_COLLECTION).EnsureIndex(index); err != nil {
			return err
		}
	}

	err := db.C(AUTH_COLLECTION).Insert(&account)
	return err
}

// Delete an existing User
func (m *AuthsDAO) Delete(account Account) error {
	err := db.C(AUTH_COLLECTION).Remove(&account)
	return err
}

// Update an existing User
func (m *AuthsDAO) Update(account Account) error {
	err := db.C(AUTH_COLLECTION).UpdateId(account.ID, &account)
	return err
}
